
public class Monkey{
    public String name;
    public String occupation;
    public boolean isSilly;

    public void treeClimbing(){
        if (isSilly){
            System.out.println(name + " is attempting to climb a tree! Will he succeed?");
            System.out.println("YES ! " + name + " skillfully climbed his tree!");
        } else {
            System.out.println(name + " isn't silly enough to climb the tree.");
        }
    }
    public void bananaStealing(){
        System.out.println(name + " decided to drop it's occupation of " + occupation + " to steal bananas instead");
        System.out.println("He successfully stole 5 bananas");
    }

}