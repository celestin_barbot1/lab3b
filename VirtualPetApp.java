import java.util.Scanner;
public class VirtualPetApp{
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Monkey[] tribe = new Monkey[4];
        for(int i = 0; i<tribe.length; i++){
            tribe[i] = new Monkey();
            System.out.println("A new monkey joined the tribe, what will you name him?");
            tribe[i].name = reader.nextLine();
            System.out.println(tribe[i].name + " that's a great choice! Now how does this monkey spends it's time?");
            tribe[i].occupation = reader.nextLine();
            System.out.println(tribe[i].occupation + " wow that's an interesting way to pass time! Now finally for the last part... Is " + tribe[i].name + " a silly monkey? \nEnter true or false:");
            tribe[i].isSilly = reader.nextLine().toLowerCase().equals("true");
        }
        System.out.println(tribe[tribe.length - 1].isSilly);
        System.out.println(tribe[tribe.length - 1].name);
        System.out.println(tribe[tribe.length - 1].occupation);


        tribe[0].bananaStealing();
        tribe[0].treeClimbing();

    }
}